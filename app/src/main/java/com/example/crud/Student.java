package com.example.crud;

public class Student {
    String id;
    String name;
    String email;

    public Student(String id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }
}
