package com.example.crud;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    EditText editTextId, editTextName, editTextEmail;
    Button buttonInsert, buttonGetStudents, buttonGetStudentById, buttonUpdateStudent, buttonDeleteStudents, buttonDeleteStudentById;
    DatabaseHelper databaseHelper;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = getApplicationContext();
        databaseHelper = new DatabaseHelper(this);
        editTextId = (EditText) findViewById(R.id.editTextId);
        editTextName = (EditText) findViewById(R.id.editTextName);
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        buttonInsert = (Button) findViewById(R.id.buttonInsert);
        buttonGetStudents = (Button) findViewById(R.id.buttonGetStudents);
        buttonGetStudentById = (Button) findViewById(R.id.buttonGetStudentById);
        buttonUpdateStudent = (Button) findViewById(R.id.buttonUpdateStudent);
        buttonDeleteStudents = (Button) findViewById(R.id.buttonDeleteStudents);
        buttonDeleteStudentById = (Button) findViewById(R.id.buttonDeleteStudentById);
        buttonInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = editTextName.getText().toString();
                String email = editTextEmail.getText().toString();

                databaseHelper.insertStudent(name, email);
            }
        });

        buttonGetStudents.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("Range")
            @Override
            public void onClick(View v) {
                StringBuilder stringBuilder = new StringBuilder();
                Cursor cursor = databaseHelper.getStudents();
                while (cursor.moveToNext()) {


                    stringBuilder.append("Id: " + cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_ID)) + '\n');
                    stringBuilder.append("Name: " + cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_NAME)) + '\n');
                    stringBuilder.append("Email: " + cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_EMAIL)) + '\n' + '\n');
                }
                showMessage("all students", stringBuilder.toString());
            }
        });

        buttonGetStudentById.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("Range")
            @Override
            public void onClick(View v) {
                String id = editTextId.getText().toString();
                Cursor cursor = databaseHelper.getStudentById(id);
                cursor.moveToNext();
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Id: " + cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_ID)) + '\n');
                stringBuilder.append("Name: " + cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_NAME)) + '\n');
                stringBuilder.append("Email: " + cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_EMAIL)) + '\n');
                showMessage("student by id: " + id, stringBuilder.toString());
            }
        });

        buttonUpdateStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Student> students = getStudentArrayList();
                String id = editTextId.getText().toString();
                String name = editTextName.getText().toString();
                String email = editTextEmail.getText().toString();
                boolean isIdExists = false;
                for (Student st : students) {
                    if (st.id.equals(id)) isIdExists = true;
                }
                if (isIdExists) {
                    databaseHelper.updateStudent(id, name, email);
                } else {
                    Toast.makeText(context, "id is not exist", Toast.LENGTH_LONG).show();
                }

            }
        });

        buttonDeleteStudents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseHelper.deleteAllStudents();
            }
        });

        buttonDeleteStudentById.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = editTextId.getText().toString();
                databaseHelper.deleteStudentById(id);
            }
        });
    }

    public void showMessage(String title, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.create();
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.show();
    }

    @SuppressLint("Range")
    public ArrayList<Student> getStudentArrayList() {
        Cursor cursor = databaseHelper.getStudents();
        ArrayList<Student> students = new ArrayList<>();
        while (cursor.moveToNext()) {
            students.add(new Student(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_ID)),
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_NAME)),
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_EMAIL))));
        }
        return students;
    }
}